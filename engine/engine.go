package engine

import (
	"context"
	"fmt"
	"github.com/go-pg/pg"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"time"
)

var log = logging.MustGetLogger("bistox")
const ChannelBufferSize = 16

type Engine struct {
	orderBook *OrderBook
	consume chan Order
	cancel chan Order
	produce chan []Trade

	ctx context.Context
	service *bistox_core.Service
	db *pg.DB
}

type Trade = models.Trade
type Wallet = models.Wallet

type TradeAbortion struct {
	Order *Order
	Amount int64
}

func (engine *Engine) initFromDb() error {
	db := engine.db.WithContext(engine.ctx)
	var orders []Order

	log.Info("Initializing order book from db")

	err := db.Model(&orders).
		Where("deleted_at is null").
		Select()

	if err == pg.ErrNoRows {
		log.Info("No orders in db, nothing to initialize")
	} else if err != nil {
		log.Errorf("Could not initialize order book from db: %s", err)
		return err
	}

	for i := range orders {
		baseCurrency := orders[i].BaseCurrencyCode
		quotedCurrency := orders[i].QuotedCurrencyCode
		if orders[i].OrderType == models.Limit {
			if orders[i].OperationType == models.Buy {
				engine.orderBook.LimitBuyOrders[baseCurrency][quotedCurrency].addOrder(orders[i])
			} else if orders[i].OperationType == models.Sell {
				engine.orderBook.LimitSellOrders[baseCurrency][quotedCurrency].addOrder(orders[i])
			}
		} else if orders[i].OrderType == models.Market {
			if orders[i].OperationType == models.Buy {
				engine.orderBook.MarketBuyOrders[baseCurrency][quotedCurrency].addOrder(orders[i])
			} else if orders[i].OperationType == models.Sell {
				engine.orderBook.MarketSellOrders[baseCurrency][quotedCurrency].addOrder(orders[i])
			}
		}
	}
	return nil
}

func (engine *Engine) Process(order Order) {
	engine.consume <- order
}

func (engine *Engine) Cancel(order Order) {
	engine.cancel <- order
}

func (engine *Engine) matchOrders() {
	for {
		select {
		case <-engine.ctx.Done():
			log.Warning("ctx.Done() received, stopping matching orders")
			break
		case order := <-engine.consume:
			trades := engine.processOrder(order)
			if len(trades) > 0 {
				engine.produce <- trades
			} else {
				log.Debug("No trades were produced")
			}
		case order := <- engine.cancel:
			err := engine.cancelOrder(order)
			if err != nil {
				log.Error(err)
			} else {
				log.Debugf("Order %d was successfully deleted!", order.ID)
			}
		}
	}
}

func (engine *Engine) processOrder(order Order) []Trade {
	log.Debugf("Processing order %d", order.ID)
	var trades []Trade
	if order.OperationType == models.Buy {
		trades = engine.processBuyOrder(order)
	} else if order.OperationType == models.Sell {
		trades = engine.processSellOrder(order)
	}
	return trades
}

func makeTrade(maker *Order, taker *Order) Trade {
	trade := Trade{
		Maker: &Order{ID: maker.ID, BaseCurrencyCode: maker.BaseCurrencyCode},
		MakerId: maker.ID,
		Taker: &Order{ID: taker.ID},
		TakerId: taker.ID,
		Price: maker.Price,
	}
	if maker.Left() < taker.Left() {
		trade.Amount = maker.Left()
	} else {
		trade.Amount = taker.Left()
	}
	maker.FilledAmount += trade.Amount
	maker.FilledPrice += trade.TotalCost()
	taker.FilledAmount += trade.Amount
	taker.FilledPrice += trade.TotalCost()
	return trade
}

func (engine *Engine) processBuyOrder(order Order) []Trade {
	marketBuyOrders := engine.orderBook.MarketBuyOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
	limitBuyOrders := engine.orderBook.LimitBuyOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
	marketSellOrders := engine.orderBook.MarketSellOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
	limitSellOrders := engine.orderBook.LimitSellOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
	trades := make([]Trade, 0)

	if order.OrderType == models.Limit {
		for e := marketSellOrders.Back(); e != nil; {
			taker := e.Value.(Order)
			trades = append(trades, makeTrade(&order, &taker))

			if taker.Left() == 0 {
				oldE := e
				e = e.Prev()
				marketSellOrders.Remove(oldE)
			} else {
				e.Value = taker
			}

			if order.Left() == 0 {
				return trades
			}
		}
	}

	for e := limitSellOrders.Back(); e != nil; {
		maker := e.Value.(Order)
		if order.OrderType == models.Market || maker.Price <= order.Price {
			trades = append(trades, makeTrade(&maker, &order))

			if maker.Left() == 0 {
				oldE := e
				e = e.Prev()
				limitSellOrders.Remove(oldE)
			} else {
				e.Value = maker
			}

			if order.Left() == 0 {
				return trades
			}
		} else {
			break
		}
	}

	if order.Left() > 0 {
		if order.OrderType == models.Market {
			marketBuyOrders.addOrder(order)
		} else if order.OrderType == models.Limit {
			limitBuyOrders.addOrder(order)
		}
	}

	return trades
}

func (engine *Engine) processSellOrder(order Order) []Trade {
	marketSellOrders := engine.orderBook.MarketSellOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
	limitSellOrders := engine.orderBook.LimitSellOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
	marketBuyOrders := engine.orderBook.MarketBuyOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
	limitBuyOrders := engine.orderBook.LimitBuyOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
	trades := make([]Trade, 0)

	if order.OrderType == models.Limit {
		for e := marketBuyOrders.Back(); e != nil; {
			taker := e.Value.(Order)
			trades = append(trades, makeTrade(&order, &taker))

			if taker.Left() == 0 {
				oldE := e
				e = e.Prev()
				marketBuyOrders.Remove(oldE)
			} else {
				e.Value = taker
			}

			if order.Left() == 0 {
				return trades
			}
		}
	}

	for e := limitBuyOrders.Back(); e != nil; {
		maker := e.Value.(Order)
		if order.OrderType == models.Market || maker.Price >= order.Price {
			trades = append(trades, makeTrade(&maker, &order))

			if maker.Left() == 0 {
				oldE := e
				e = e.Prev()
				limitBuyOrders.Remove(oldE)
			} else {
				e.Value = maker
			}

			if order.Left() == 0 {
				return trades
			}
		} else {
			break
		}
	}

	if order.Amount > 0 {
		if order.OrderType == models.Market {
			marketSellOrders.addOrder(order)
		} else if order.OrderType == models.Limit {
			limitSellOrders.addOrder(order)
		}
	}

	return trades
}

func (engine *Engine) processTrades() {
	for {
		select {
		case <-engine.ctx.Done():
			log.Warning("ctx.Done() received, stopping processing trades")
			return
		case trades := <-engine.produce:
			engine.applyTrades(trades)
		}
	}
}

func (engine *Engine) applyTrades(trades []Trade) {
	log.Debug("Applying trades")

	//var abortions map[uint64]TradeAbortion
	//var ordersToRemove map[uint64]*Order
	for _, trade := range trades {
		log.Debugf("Applying trade: %#v", trade)

		var maker Order
		trade.Maker = &maker
		err := engine.db.Model(&maker).
			Where("id = ?", trade.MakerId).
			For("UPDATE").
			Select()
		if err != nil {
			log.Error(err)
			return
		}

		var makerBaseWallet Wallet
		trade.Maker.BaseWallet = &makerBaseWallet
		err = engine.db.Model(&makerBaseWallet).
			Where("id = ?", maker.BaseWalletId).
			For("UPDATE").
			Select()
		if err != nil {
			log.Error(err)
			return
		}

		var makerQuotedWallet Wallet
		trade.Maker.QuotedWallet = &makerQuotedWallet
		err = engine.db.Model(&makerQuotedWallet).
			Where("id = ?", maker.QuotedWalletId).
			For("UPDATE").
			Select()
		if err != nil {
			log.Error(err)
			return
		}

		var taker Order
		trade.Taker = &taker
		err = engine.db.Model(&taker).
			Where("id = ?", trade.TakerId).
			For("UPDATE").
			Select()
		if err != nil {
			log.Error(err)
			return
		}

		var takerBaseWallet Wallet
		trade.Taker.BaseWallet = &takerBaseWallet
		err = engine.db.Model(&takerBaseWallet).
			Where("id = ?", taker.BaseWalletId).
			For("UPDATE").
			Select()
		if err != nil {
			log.Error(err)
			return
		}

		var takerQuotedWallet Wallet
		trade.Taker.QuotedWallet = &takerQuotedWallet
		err = engine.db.Model(&takerQuotedWallet).
			Where("id = ?", taker.QuotedWalletId).
			For("UPDATE").
			Select()
		if err != nil {
			log.Error(err)
			return
		}

		trade.Maker.FilledAmount += trade.Amount
		trade.Maker.FilledPrice += trade.TotalCost()
		trade.Taker.FilledAmount += trade.Amount
		trade.Taker.FilledPrice += trade.TotalCost()
		now := time.Now()
		if trade.Maker.Left() == 0 {
			trade.Maker.DeletedAt = &now
		}
		if trade.Taker.Left() == 0 {
			trade.Taker.DeletedAt = &now
		}
		if trade.Maker.OperationType == models.Buy {
			trade.Maker.BaseWallet.Balance += trade.Amount
			trade.Maker.QuotedWallet.Balance -= trade.TotalCost()
			trade.Taker.BaseWallet.Balance -= trade.Amount
			trade.Taker.QuotedWallet.Balance += trade.TotalCost()
		} else if trade.Maker.OperationType == models.Sell {
			trade.Maker.BaseWallet.Balance -= trade.Amount
			trade.Maker.QuotedWallet.Balance += trade.TotalCost()
			trade.Taker.BaseWallet.Balance += trade.Amount
			trade.Taker.QuotedWallet.Balance -= trade.TotalCost()
		}
		_, err = engine.db.Model(&trade).Insert()
		if err != nil {
			log.Error(err)
			return
		}

		_, err = engine.db.Model(trade.Maker, trade.Taker).
			Update()
		if err != nil {
			log.Error(err)
			return
		}
		_, err = engine.db.Model(trade.Maker.BaseWallet, trade.Maker.QuotedWallet, trade.Taker.BaseWallet, trade.Taker.QuotedWallet).
			Column("balance").
			Update()
		if err != nil {
			log.Error(err)
			return
		}

		//if maker.OperationType == models.Buy {
		//	maker.Failed = maker.Failed || makerQuotedWallet.Balance < trade.TotalPrice()
		//	taker.Failed = taker.Failed || takerBaseWallet.Balance < trade.Amount
		//} else if maker.OperationType == models.Sell {
		//	maker.Failed = maker.Failed || makerBaseWallet.Balance < trade.Amount
		//	taker.Failed = taker.Failed || takerQuotedWallet.Balance < trade.TotalPrice()
		//}
		//
		//if maker.Invalid() && !taker.Invalid() {
		//	log.Warning("Taker %#v is invalid, aborting trade", taker)
		//	abortion, ok := abortions[taker.ID]
		//	if !ok {
		//		abortion.Order = &taker
		//	}
		//	abortion.Amount += trade.Amount
		//	abortions[taker.ID] = abortion
		//} else if !maker.Invalid() && taker.Invalid() {
		//	log.Warning("Maker %#v is invalid, aborting trade", maker)
		//	abortion, ok := abortions[maker.ID]
		//	if !ok {
		//		abortion.Order = &maker
		//	}
		//	abortion.Amount += trade.Amount
		//	abortions[maker.ID] = abortion
		//}
		//
		//now := time.Now()
		//if maker.Failed {
		//	log.Warning("Maker %#v has failed, removing it", maker)
		//	ordersToRemove[maker.ID] = &maker
		//	delete(abortions, maker.ID)
		//	if maker.DeletedAt == nil {
		//		maker.DeletedAt = new(time.Time)
		//		*maker.DeletedAt = now
		//	}
		//}
		//if taker.Failed {
		//	log.Warning("Taker %#v has failed, removing it", taker)
		//	ordersToRemove[taker.ID] = &taker
		//	delete(abortions, taker.ID)
		//	if taker.DeletedAt == nil {
		//		taker.DeletedAt = new(time.Time)
		//		*taker.DeletedAt = now
		//	}
		//}
		//
		//if !maker.Invalid() && !taker.Invalid() {
		//	maker.FilledAmount += trade.Amount
		//	maker.FilledPrice += trade.TotalPrice()
		//	taker.FilledAmount += trade.Amount
		//	taker.FilledPrice += trade.TotalPrice()
		//	if maker.OperationType == models.Buy {
		//		makerBaseWallet.Balance += trade.Amount
		//		makerQuotedWallet.Balance -= trade.TotalPrice()
		//		takerBaseWallet.Balance -= trade.Amount
		//		takerQuotedWallet.Balance += trade.TotalPrice()
		//	} else if maker.OperationType == models.Sell {
		//		makerBaseWallet.Balance -= trade.Amount
		//		makerQuotedWallet.Balance += trade.TotalPrice()
		//		takerBaseWallet.Balance += trade.Amount
		//		takerQuotedWallet.Balance -= trade.TotalPrice()
		//	}
		//	_, err = engine.db.Model(&trade).Insert()
		//	if err != nil {
		//		log.Error(err)
		//		return
		//	}
		//}

		//_, err = engine.db.Model(&maker, &taker).
		//	Update()
		//if err != nil {
		//	log.Error(err)
		//	return
		//}
		//_, err = engine.db.Model(&makerBaseWallet, &makerQuotedWallet, &takerBaseWallet, &takerQuotedWallet).
		//	Column("balance").
		//	Update()
		//if err != nil {
		//	log.Error(err)
		//	return
		//}
	}
}

func (engine *Engine) cancelOrder(order Order) error {
	var orderList *OrderList
	if order.OrderType == models.Limit {
		if order.OperationType == models.Buy {
			orderList = engine.orderBook.LimitBuyOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
		} else if order.OperationType == models.Sell {
			orderList = engine.orderBook.LimitSellOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
		}
	} else if order.OrderType == models.Market {
		if order.OperationType == models.Buy {
			orderList = engine.orderBook.MarketBuyOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
		} else if order.OperationType == models.Sell {
			orderList = engine.orderBook.MarketSellOrders[order.BaseCurrencyCode][order.QuotedCurrencyCode]
		}
	}

	for e := orderList.Back(); e != nil; e = e.Prev(){
		o := e.Value.(Order)

		if o.ID == order.ID {
			orderList.Remove(e)
			return nil
		}
	}
	return fmt.Errorf("order %d was not found in error book", order.ID)
}

func (engine *Engine) Start(ctx context.Context) error {
	log.Info("Starting matching engine")

	engine.ctx = ctx
	engine.db = engine.service.DB()
	err := engine.initFromDb()
	if err != nil {
		return err
	}

	engine.consume = make(chan Order, ChannelBufferSize)
	engine.cancel = make(chan Order, ChannelBufferSize)
	engine.produce = make(chan []Trade, ChannelBufferSize)

	go engine.matchOrders()
	go engine.processTrades()
	go engine.saveTradeStates()
	return nil
}

func NewEngine(service *bistox_core.Service) *Engine {
	orderBook := &OrderBook{
		LimitBuyOrders: make(map[Currency]map[Currency]*OrderList),
		LimitSellOrders: make(map[Currency]map[Currency]*OrderList),
		MarketBuyOrders: make(map[Currency]map[Currency]*OrderList),
		MarketSellOrders: make(map[Currency]map[Currency]*OrderList),
	}
	for _, pair := range models.ValidPairs {
		if orderBook.LimitBuyOrders[pair.BaseCurrency] == nil {
			orderBook.LimitBuyOrders[pair.BaseCurrency] = make(map[Currency]*OrderList)
		}
		if orderBook.LimitSellOrders[pair.BaseCurrency] == nil {
			orderBook.LimitSellOrders[pair.BaseCurrency] = make(map[Currency]*OrderList)
		}
		if orderBook.MarketBuyOrders[pair.BaseCurrency] == nil {
			orderBook.MarketBuyOrders[pair.BaseCurrency] = make(map[Currency]*OrderList)
		}
		if orderBook.MarketSellOrders[pair.BaseCurrency] == nil {
			orderBook.MarketSellOrders[pair.BaseCurrency] = make(map[Currency]*OrderList)
		}

		orderBook.LimitBuyOrders[pair.BaseCurrency][pair.QuotedCurrency] = newOrderList(func(lhs, rhs Order) bool {
			return lhs.Price < rhs.Price ||
				(lhs.Price == rhs.Price && lhs.CreatedAt.After(rhs.CreatedAt))
		})
		orderBook.LimitSellOrders[pair.BaseCurrency][pair.QuotedCurrency] = newOrderList(func(lhs, rhs Order) bool {
			return lhs.Price > rhs.Price ||
				(lhs.Price == rhs.Price && lhs.CreatedAt.After(rhs.CreatedAt))
		})
		orderBook.MarketBuyOrders[pair.BaseCurrency][pair.QuotedCurrency] = newOrderList(func(lhs, rhs Order) bool {
			return lhs.CreatedAt.After(rhs.CreatedAt)
		})
		orderBook.MarketSellOrders[pair.BaseCurrency][pair.QuotedCurrency] = newOrderList(func(lhs, rhs Order) bool {
			return lhs.CreatedAt.After(rhs.CreatedAt)
		})
	}

	return &Engine{
		orderBook: orderBook,
		service: service,
	}
}
