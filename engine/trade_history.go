package engine

import (
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"time"
)

const historySaveInterval = 10 * time.Second


func (engine *Engine) saveTradeStates() {
	ticker := time.NewTicker(historySaveInterval)
	for {
		select {
		case <-ticker.C:
		}

		for _, pair := range models.ValidPairs {
			maxBuy := engine.orderBook.LimitBuyOrders[pair.BaseCurrency][pair.QuotedCurrency].Back()
			minSell := engine.orderBook.LimitSellOrders[pair.BaseCurrency][pair.QuotedCurrency].Back()
			if maxBuy == nil || minSell == nil {
				continue
			}

			price := (maxBuy.Value.(Order).Price + minSell.Value.(Order).Price) / 2
			var volume int64

			for buy := maxBuy; buy != nil; buy = buy.Prev() {
				volume += buy.Value.(Order).Amount
			}

			history := &models.TradeHistory{
				BaseCurrencyCode: pair.BaseCurrency,
				QuotedCurrencyCode: pair.QuotedCurrency,
				Price: price,
				Volume: volume,
			}

			if err := engine.db.Insert(history); err != nil {
				log.Error(err)
			}
		}
	}
}
