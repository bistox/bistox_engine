package engine

import (
	"container/list"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)

type Order = models.Order
type OrderComp = func(lhs, rhs Order) bool
type Currency = models.CurrencyEnum

type OrderList struct {
	list.List
	less OrderComp
}

func newOrderList(comp OrderComp) *OrderList {
	return &OrderList{less: comp}
}

func (ol *OrderList) addOrder(order Order) {
	for e := ol.Back(); e != nil; e = e.Prev() {
		if !ol.less(order, e.Value.(Order)) {
			ol.InsertAfter(order, e)
			return
		}
	}
	ol.PushFront(order)
}

//from/to currency mapping
type OrderBook struct {
	LimitBuyOrders   map[Currency]map[Currency]*OrderList
	LimitSellOrders  map[Currency]map[Currency]*OrderList
	MarketBuyOrders  map[Currency]map[Currency]*OrderList
	MarketSellOrders map[Currency]map[Currency]*OrderList
}