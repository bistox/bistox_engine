package handlers

import (
	"context"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"gitlab.com/bistox/bistox_engine/engine"
	pb "gitlab.com/bistox/bistox_proto"
	"gitlab.com/bistox/bistox_proto/engine"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

const HandlerMaxTime = 2000 * time.Millisecond
var log = logging.MustGetLogger("bistox")

type EngineServer struct {
	service *bistox_core.Service
	engine *engine.Engine
}

func (es *EngineServer) CreateOrder(ctx context.Context, req *proto_engine.CreateOrderRequest) (*pb.Order, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	log.Debug("Creating new order...")

	db := es.service.DB().WithContext(ctx)

	order := models.Order{
		OrderType: 			models.OrderType(req.OrderType.String()),
		OperationType: 		models.OperationType(req.OperationType.String()),
		BaseCurrencyCode: 	models.CurrencyEnum(req.BaseCurrency.String()),
		QuotedCurrencyCode: models.CurrencyEnum(req.QuotedCurrency.String()),
		Amount: 			req.Amount,
		Price: 				req.Price,
		BaseWalletId: 		req.BaseWalletId,
		QuotedWalletId: 	req.QuotedWalletId,
		ProfileId:			req.ProfileId,
	}

	if err := db.Insert(&order); err != nil {
		log.Errorf("Error while inserting order %v - %v", order, err)
		return nil, status.Error(codes.Internal, "Internal error")
	}

	es.engine.Process(order)

	log.Debugf("Order %d was successfully created!", order.ID)
	return order.ToProto(), nil
}

func (es *EngineServer) CancelOrder(ctx context.Context, req *proto_engine.CancelOrderRequest) (*proto_engine.CancelOrderResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, HandlerMaxTime)
	defer cancel()

	log.Debugf("Cancelling order %d...", req.OrderId)

	db := es.service.DB().WithContext(ctx)

	order := models.Order{}
	err := db.Model(&order).
		Where("id = ?", req.OrderId).
		For("UPDATE").
		Select()
	if err != nil {
		log.Error(err)
		return nil, status.Errorf(codes.Internal, "Internal error")
	}

	es.engine.Cancel(order)

	now := time.Now()
	order.Canceled = true
	order.DeletedAt = &now

	err = db.Update(&order)
	if err != nil {
		log.Error(err)
		return nil, status.Errorf(codes.Internal, "Internal error")
	}

	return &proto_engine.CancelOrderResponse{}, nil
}

// Force check interface implementation
var _ proto_engine.BistoxEngineServer = (*EngineServer)(nil)

func NewEngineServer(service *bistox_core.Service, engine *engine.Engine) EngineServer {
	return EngineServer{service: service, engine: engine}
}