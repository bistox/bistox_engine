package main

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core"
	"gitlab.com/bistox/bistox_core/mods/server"
	"gitlab.com/bistox/bistox_engine/engine"
	"gitlab.com/bistox/bistox_engine/handlers"
	pb "gitlab.com/bistox/bistox_proto/engine"
	"google.golang.org/grpc"
	"os"
)

//go:generate make protogen

var log = logging.MustGetLogger("bistox")

func main() {
	service := bistox_core.NewService(logging.DEBUG)
	service.Init()

	engine := engine.NewEngine(&service)
	engineServer := handlers.NewEngineServer(&service, engine)

	service.Config.Server.Ext = server.ConfigExt{
		RpcRegHandler: func(s *grpc.Server, mux *runtime.ServeMux) {
			pb.RegisterBistoxEngineServer(s, &engineServer)
		},
	}

	service.OnReady(func(ctx context.Context) {
		if err := engine.Start(ctx); err != nil {
			service.Kill()
		}
	})

	if err := service.Run(); err != nil {
		os.Exit(1)
	}
}
